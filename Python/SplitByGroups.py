import hou
selected = hou.selectedNodes()[0]
groups = [g.name() for g in selected.geometry().primGroups()]
for i, name in enumerate(groups, 1):
        #make a split node
        split = selected.createOutputNode("blast")
        split.setName(name)
        split.parm("group").set(name)
        split.parm("negate").set(1)
        split.moveToGoodPosition()




for node in selected:
        out = node.createOutputNode("null")
        out.setName("OUT_"+node)
        out.moveToGoodPosition()
